\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}  
\usepackage[T1]{fontenc} 
\usepackage[english,francais]{babel}
\usepackage{color}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amscd}
\usepackage{amsfonts,amsmath,amsthm,amssymb,stmaryrd} 
\usepackage{graphicx}
\usepackage{pstricks,pst-plot,pstricks-add}
%\usepackage{enumitem}

\begin{document}  

\title{Probability - The Science of Uncertainty and Data\\ Unit 1 - part 1 \\Probability models and axioms}
\author{Herlin Simon}
\date{}

\maketitle
\newpage
\section{Probability models and axioms}
\subsection{Sample space - Exercice}
For the experiment of flipping a coin, and for each one of the following choices, determine whether we have a legitimate sample space:\\\\
$\Omega = \{$ Heads and it is raining, Heads and it is not raining, Tails $\}$ : Yes\\
In the first case, the elements of $\Omega$ are mutually exclusive and collectively exhaustive, and therefore $\Omega$ is a legitimate sample space.\\\\
$\Omega = \{$ Heads and it is raining, Tails and it is not raining, Tails $\}$ : No\\
For the second case, if the outcome is “Tails and it is not raining," then the outcome “Tails" will have also occurred. Therefore the elements of $\Omega$ are not mutually exclusive, and $\Omega$ is not a legitimate sample space. 

\subsection{Tree representations - Exercice}
Paul checks the weather forecast. If the forecast is good, Paul will go out for a walk. If the forecast is bad, then Paul will either stay home or go out. If he goes out, he might either remember or forget his umbrella. In the tree diagram below, identify the leaf that corresponds to the event that the forecast is bad and Paul stays home.\\
\begin{center} \includegraphics[scale=0.54]{./img/tree-exercice.jpg} \end{center}
Answer : 2\\
Under a bad forecast, there are mutliple scenarios, so a bad forecast must correspond to starting along the lower branch. Furthermore, if Paul goes out, there are two further possibilities, and these must correspond to leaves 3 and 4. Therefore, the event of staying home must correspond to leaf 2. 

\newpage
\subsection{Axioms - Exercice}
Let $A$ and $B$ be events on the same sample space, with $P(A) = 0.6$ and $P(B) = 0.7$. Can these two events be disjoint ?\\
Answer = No.\\
If the two events were disjoint, the additivity axiom would imply that $P(A \cup B) = P(A) + P(B) = 1.3 > 1$, which would contradict the normalization axiom. \\

\subsection{Simple properties of probabilities}
\textbf{Axioms : }
\begin{itemize}
	\item $P(A) \geq 0$
	\item $P(B) = 1$ 
\end{itemize}
\textbf{Consequence : }
\begin{itemize}
	\item $P(A) \leq 1$
	\item $P(\emptyset) = 0$
	\item $P(A) + P(A^c) = 1$
\end{itemize}
\textbf{For disjoint events :}
\begin{itemize}
	\item $P(A \cup B) = P(A) + P(B)$
	\item $P(A \cup B \cup C) = P(A) + P(B) + P(C)$ and similary for $k$ disjoint events
\end{itemize}
\textbf{Some simple consequence of the axioms : }
\begin{itemize}
	\item $A \cup A^c = \Omega$	
	\item $A \cap A^c = \emptyset$
	\item if $A_1, ..., A_k$ disjoint $\rightarrow \overset{k}{\underset{i = 1}{\sum}}P(A_i)$ 
\end{itemize}

\newpage
\subsection{Simple properties - Exercice}
Let $A$, $B$, and $C$ be disjoint subsets of the sample space. For each one of the following statements, determine whether it is true or false.\\\\

$P(A) + P(A^c) + P(B) = P(A \cup A^c \cup B)$\\
Answer : False\\
For a counterexample, let $A=\emptyset, B=\Omega$, and $C=\emptyset$. In that case, the left-hand side of the equation equals 2, whereas the right-hand side equals 1.\\
\\
$P(A) + P(B) \leq 1$\\
Answer : Right\\
\\
$P(A^c) + P(B) \leq 1$\\
Answer : False\\
For a counterexample, let $A=\emptyset, B=\Omega$ and $c=\emptyset$. In that case, $P(A^c) + P(B) = 2$.\\
\\
$P(A \cup B \cup C) \geq P(A \cup B)$\\
Answer : True\\
\\
\newpage
\subsection{More properties of probabilities}
\begin{itemize}
	\item If $A \subset B$ then $P(A) \leq P(B)$
	\item $P(A \cup B) = P(A) + P(B) - P(A \cap B)$
	\item $P(A \cup B) \leq P(A) + P(B)$
	\item $P(A \cup B \cup C) = P(A) + P(A^c \cap B) + P(A^c \cap B^c \cap C)$
\end{itemize}
\subsection{More properties of probabilities - Exercice}
Let $A$, $B$, and $C$ be subsets of the sample space, not necessarily disjoint. For each one of the following statements, determine whether it is true or false.\\
\\
$P((A \cap B) \cup (C \cap A^c)) \leq P(A \cup B \cup C)$\\
Answer : True\\
This is because the set $(A \cap B) \cup (C \cap A^c)$ is a subset of $A \cup B \cup C$. \\
\\
$P(A \cup B \cup C) = P(A \cap C^c) + P(C) + P(B \cap A^c \cap C^c)$\\
Answer : True\\
This is the same property shown in the last segment, with the three sets appearing in a different order.\\
\newpage
\subsection{Discret probability calculations - Exercice}
Consider the same model of two rolls of a tetrahedral die, with all 16 outcomes equally likely. Find the probability of the following events:\\
\\
The value in the first roll is strictly larger than the value in the second roll.\\
Answer : $\frac{3}{8}$\\
The event of interest is $\{(2,1), (3,1), (4,1), (3,2), (4,2), (4,3)\}$. It consists of 6 elements (outcomes), each of which has probability $1/16$, for an overall probability of $6/16=3/8$. \\
\\
The sum of the values obtained in the two rolls is an even number.\\
Answer : $\frac{1}{2}$\\
The event of interest is $\{(1,1), (2,2), (3,3), (4,4), (1,3), (3,1), (2,4), (4,2)\}$. It consists of 8 elements (outcomes), each of which has probability $1/16$, for an overall probability of $8/16=1/2$.
\newpage
\subsection{Continuous probability calculations - Exercice} 
Consider a sample space that is the rectangular region $[0,1] * [0,2]$, i.e., the set of all pairs $(x, y)$ that satisfy $a \leq x \leq 1$ and $0 \leq y \leq 2$. Consider a “uniform" probability law, under which the probability of an event is half of the area of the event. Find the probability of the following events:\\
\\
The two components $x$ and $y$ have the same values.\\
Answer : 0\\
This event is a line, and since a line has zero area, the probability is zero.\\
\\
The value, $x$, of the first component is larger than or equal to the value, $y$, of the second component.\\
Answer : $\frac{1}{4}$\\
 This event is a triangle with vertices at $(0,0), (1,0), (1,1)$. Its area is $1/2$, and therefore the probability is $1/4$.\\
\\
The value of $x^2$ is larger or equal to the value of y.\\
Answer : $\frac{1}{6}$\\
This event corresponds to the region below the curve $y=x^2$, where $x$ ranges from 0 to 1. The area of this region is $ \overset{1}{\underset{0}{\int}} x^2 dx = \frac{1}{•3}$ and therefore the corresponding probability is $\frac{1}{6}$
\newpage
\subsection{Countable additivity}
\subsubsection{Probability calculation : discrete but infinite sample space}
\begin{itemize}
	\item Sample space : $\{1, 2, ...\}$ \\
		We are given $P(n) = \frac{1}{2^n}$, $n = 1, 2, ...$\\
		$\overset{\infty}{\underset{n = 1}{\sum}} \frac{1}{2^n} = \frac{1}{2} \overset{\infty}{\underset{n = 0}{\sum}} \frac{1}{2^n} = \frac{1}{2} * \frac{1}{1 - \frac{1}{2}} = 1$
	\item $P($outcome is even$) = P(\{2, 4, 6, ...\})$\\
	$ = P({2} \cup {4} \cup {6}, ...) = P(2) + P(4) + P(6) ...$\\
	$ = \frac{1}{2^2} + \frac{1}{2^4} + \frac{1}{2^6} = \frac{1}{4} * (1 + \frac{1}{4} + \frac{1}{4^2} + ...) = \frac{1}{4} * \frac{1}{1 - \frac{1}{4}} = \frac{1}{3}$
\end{itemize}
\subsection{Using countable additivity - Exercice}
Let the sample space be the set of positive integers and suppose that $P(n) = \frac{1}{2^n}$, for $n=1, 2,...$. Find the probability of the set $\{3,6,9,...\}$, that is, of the set of of positive integers that are multiples of $3$.\\
\\
Answer : $\frac{1}{7}$\\
Using countable additivity, and with $\alpha = 2 - 3 = \frac{1}{8}$, the desired probability is \\
$$\frac{1}{2^3} + \frac{1}{2^6} + \frac{1}{2^9} ... = \alpha + \alpha^2 + \alpha^3 ... = \frac{\alpha}{1 - \alpha} = \frac{\frac{1}{8}}{1 - \frac{1}{8}} = \frac{1}{7}$$
Let the sample space be the set of all positive integers. Is it possible to have a “uniform" probability law, that is, a probability law that assigns the same probability $c$ to each positive integer?\\
Answer : No
Suppose that $c=0$. Then, by countable additivity, \\
$1 = P(\Omega) = P({1} \cup {2} \cup {3}...) = P({1})+P({2})+P({3})+...=0+0+0+...=0$,
which is a contradiction.\\
\end{document}