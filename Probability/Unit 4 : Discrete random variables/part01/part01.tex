\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}  
\usepackage[T1]{fontenc} 
\usepackage[english,francais]{babel}
\usepackage{color}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amscd}
\usepackage{amsfonts,amsmath,amsthm,amssymb,stmaryrd} 
\usepackage{graphicx}
\usepackage{pstricks,pst-plot,pstricks-add}
%\usepackage{enumitem}

\begin{document}  

\title{Probability - The Science of Uncertainty and Data\\ Unit 4 - part 1\\Probability mass functions and expectations}
\author{Herlin Simon}
\date{}

\maketitle
\newpage
\section{Exercise: Random variables }
 Background: a number, e.g. 2, can be thought of as a trivial random variable that always takes the value 2. Let $x$ be a number.\\
Let $X$ be a random variable associated with some probabilistic experiment.\\
\\
Is it always true that $X+x$ is a random variable?\\
Answer : Yes\\
Yes. Think of a concrete example. Let $X$ be the height of a randomly selected student and let $x=10$. We are dealing with the random variable $X+10$. It is the random variable that takes the value $a+10$, whenever the random variable $X$ takes the value $a$.\\
\\
Is it always true that $X - x=0$?\\
Answer : No\\
No. Think of the same concrete example as before. The object $X-10$, where $X$ is the height of a randomly selected student, has no reason to be equal to 0. (We often use $x$ to denote the realized value of $X$. But the problem statement never said that the number $x$ considered here had any relation to the realized value of $X$.) 
\newpage
\section{Exercise: PMF calculation}
As in the previous lecture clip, consider the same example of two rolls of a 4-sided die, with all 16 outcomes equally likely. As before, let $X$ be the result of the first roll and $Y$ be the result of the second roll. Define $W=XY$. Find the numerical values of $p_W(4)$ and $p_W(5)$. \\
\\
$p_W(4) = \frac{3}{16}$\\
The event $W=4$ may occur in three different ways: $(1,4), (2,2), (4,1)$. Since all 16 outcomes of the two rolls are equally likely, $p_W(4)=P(W=4)=\frac{3}{16}$. \\
\\
$p_W(5) = 0$\\
The event $W=5$ cannot happen, and so $p_W(5)=P(W=5)=0$. 
\newpage
\section{Exercise: Random variables versus numbers}
Let $X$ be a random variable that takes integer values, with PMF $p_X(x)$. Let $Y$ be another integer-valued random variable and let $y$ be a number.\\
Is $p_X(y)$ a random variable or a number?  \\
Answer : Number\\
Recall that $p_X(.)$ is a function that maps real numbers to real numbers. So, when we give it a numerical argument, $y$, we obtain a number. \\
\\
Is $p_X(Y)$ a random variable or a number? \\
Answer : Random Variable\\
In this case, we are dealing with a function, the function being $p_X(.)$, of a random variable $Y$. And a function of a random variable is a random variable. Intuitively, the “random" value of $p_X(Y)$ is generated as follows: we observe the realized value $y$ of the random variable $Y$, and then look up the numerical value $p_X(y)$.
\newpage
\section{Exercise: Indicator variables}
Let $A$ and $B$ be two events (subsets of the same sample space $\Omega$), with nonempty intersection. Let $I_A$ and $I_B$ be the associated indicator random variables.

For each of the two cases below, select one statement that is true.

$I_A + I_B$:\\
Answer : is not the indicator random variable of any event\\
If the outcome of the experiment lies in the intersection of the events $A$ and $B$, then $I_A + I_B$ takes the value of 2. But indicator random variables can take only the values 0 or 1. Therefore, $I_A + I_B$ is not an indicator random variable.\\
\\
$I_A * I_B$:\\
Answer : is the indicator variable of the event $A \cap B$\\
 Note that $I_A * I_B$ can take only the values 0 or 1. It is equal to 1 if and only if $I_A = 1$ (i.e., event $A$ occurs) and $I_B=1$ (i.e., event $B$ occurs). Thus, $I_A * I_B$ takes the value of 1 if and only if both $A$ and $B$ occur, and so it is the indicator random variable of the event $A \cap B$.
\newpage
\section{Exercise: The binomial PMF}
You roll a fair six-sided die (all 6 of the possible results of a die roll are equally likely) 5 times, independently. Let $X$ be the number of times that the roll results in 2 or 3. Find the numerical values of the following. \\
$P_X(2.5) = 0$\\
A value of 2.5 is not possible for $X$ since the number of rolls must be an integer, and therefore $P_X(2.5)=0$. \\
\\
$P_X(1) = 0.32922$\\
For each die roll, there is a probability $2/6=1/3$ of obtaining a 2 or a 3. Hence, the random variable $X$ is binomial with parameters $n=5$ and $p=1/3$, so that $p_X(1)=(51)*(1/3)*(2/3)4 = 0.32922$. 
\newpage
\section{Exercise: Geometric random variables}
Let $X$ be a geometric random variable with parameter $p$. Find the probability that $X \geq 10$. Express your answer in terms of $p$ using standard notation.\\
$P(X \geq 10) = (1 - p)^9$\\
We can calculate the desired probability by adding the probabilities of the events $\{X=10\}, \{X=11\}, \{X=12\}$, etc., and using the formula for the sum of a geometric series. However, we can get the answer in an easier way, using the interpretation of geometric random variables as the number of trials until the first success. The event $\{X \geq 10\}$ is the event that the the first 9 trials resulted in failure, and therefore its probability is $(1 - p)^9$. 
\newpage
\section{Exercise: Expectation calculation}
The PMF of the random variable $Y$ satisfies $p_Y(-1)=1/6$, $p_Y(2)=2/6$, $P_Y(5)=3/6$, and $p_Y(y)=0$ for all other values $y$. The expected value of $Y$ is: \\
$E[Y] = (-1) * \frac{1}{6} + 2 * \frac{2}{6} +5 * \frac{3}{6} = \frac{18}{6} = 3$
\newpage
\section{Exercise: Random variables with bounded range}
Suppose a random variable $X$ can take any value in the interval $[-1,2]$ and a random variable $Y$ can take any value in the interval $[-2,3]$. \\
The random variable $X - Y$ can take any value in an interval $[a,b]$. Find the values of $a$ and $b$: \\
Answer : a = -4, b = 4\\
The smallest possible value of $X - Y$ is obtained if $X$ takes its smallest value, $-1$, and $Y$ takes its largest value, 3, resulting in $X - Y$=-1-3=-4. Similarly, the largest possible value of $X - Y$ is obtained if $X$ takes its largest value, 2, and $Y$ takes its smallest value, -2, resulting in $X - Y = 2-(-2)=4$. \\
Can the expected value of $X + Y$ be equal to 6? \\
Answer : No\\
No matter what the outcome of the experiment is, the value of $X + Y$ will be at most 5, and so the expected value can be at most 5. 
\newpage
\section{Exercise: The expected value rule}
Let $X$ be a uniform random variable on the range $\{-1,0,1,2\}$. Let $Y = X^4$. Use the expected value rule to calculate $E[Y]$. \\
$E[Y] = (-1)^4 * \frac{1}{4} + 0^4 * \frac{1}{4} + 1^4 * \frac{1}{4} + 2^4 * \frac{1}{4} = 4.5$
\newpage
\section{Exercise: Linearity of expectations}
The random variable $X$ is known to satisfy $E[X]=2$ and $E[X^2]=7$. Find the expected value of $8-X$ and of $(X-3)(X+3)$. \\
$E(X - 8] = 8 - 2 = 6$\\
$E[(X - 3)(X + 3)] = E[X^2 - 9] = 7 - 9 = -2$
\newpage
\section{Exercise: Variance calculation}
Suppose that $Var(X)=2$. The variance of $2 - 3x$ is: \\
$Var(2 - 3x) = (-3)^2 * Var(x) = 9 * 2 = 18$\\
\section{Exercise: Variance properties}
Is it always true that $E[X^2] \geq (E[X])^2$?\\
Answer : Yes\\
We know that variances are always nonnegative and that $Var(X) = E[X^2] - (E[X])^2$. Therefore, $0 \leq Var(X) = E[X^2] - (E[X])^2$, or, equivalently, $E[X^2] \geq (E[X])^2$.
\newpage
\section{Exercise: Variance of the uniform}
Suppose that the random variable $X$ takes values in the set $\{0,2,4,6,...,2n\}$ (the even integers between 0 and $2n$, inclusive), with each value having the same probability. What is the variance of $X$? Hint: Consider the random variable $Y = X/2$  and recall that the variance of a uniform random variable on the set $\{0,1,...,n\}$ is equal to $n(n+2)/12$. \\
$Var(X) = $
\newpage
\section{Exercise: Variance of the uniform}
Suppose that the random variable $X$ takes values in the set $\{0,2,4,6,...,2n\}$ (the even integers between 0 and $2n$, inclusive), with each value having the same probability. What is the variance of $X$? Hint: Consider the random variable $Y=X/2$ and recall that the variance of a uniform random variable on the set $\{0,1,...,n\}$ is equal to $n(n+2)/12$. \\
$Var(x) = Var(2Y) = 4 * Var(Y) = \frac{4}{12} * n * (n+2)$\\
\section{Exercise: Conditional variance}
In the last example, we saw that the conditional distribution of $X$, which was a uniform over a smaller range (and in some sense, less uncertain), had a smaller variance, i.e., $Var(X | A) \leq Var(X)$. Here is an example where this is not true. Let $Y$ be uniform on $\{0,1,2\}$ and let $B$ be the event that $Y$ belongs to $\{0,2\}$.\\
What is the variance of $Y$\\
$Var(Y) = 8/12$\\
The calculation of the variance of $Y$ is exactly the same as the calculation of $Var(X | A)$ in the preceding example, yielding $2/3$.\\
\\ 
What is the conditional variance $Var(Y | B)$?\\
$Var(Y | B) = 1$\\
In the conditional model, the conditional mean is $E[Y | B] = 1$. Since $Y$ is either 0 or 2 in the conditional model, the difference between $Y$ and the conditional mean is either 1 or -1, so that $(Y - E[Y|B])^2$ is always equal to 1. It follows that the conditional variance is equal to 1.\\
Note that in this example, $Var(Y|B) > Var(Y)$. 
\newpage
\section{Exercise: Total expectation calculation}
We have two coins, $A$ and $B$. For each toss of coin $A$, we obtain Heads with probability $1/2$; for each toss of coin $B$, we obtain Heads with probability $1/3$. All tosses of the same coin are independent. We select a coin at random, where the probabilty of selecting coin $A$ is $1/4$, and then toss it until Heads is obtained for the first time.\\
The expected number of tosses until the first Heads is :\\
Let $T$ be the number of tosses until the first Heads. Once a coin is selected, the conditional distribution of $T$ is geometric, with a mean of $1/p$, where $p$ is the probability of Heads for the selected coin. Let $C_A$ and $C_B$ denote the events that coin $A$ or $B$, respectively, is selected.\\
$E[T]=P(C_A)E[T∣C_A]+P(C_B)E[T∣C_B]=\frac{1}{4}*2+\frac{3}{4}*3=\frac{11}{4}$
\\
\section{Exercise: Memorylessness of the geometric}
Let $X$ be a geometric random variable, and assume that $Var(X) = 5$.\\
What is the conditional variance $Var(X - 4|X > 4)$?\\
$Var(X - 4| X > 4) = 5$\\
The conditional distribution of $X-4$ given $X>4$ is the same geometric PMF that describes the distribution of $X$. Hence $Var(X - 4 | X > 4) = Var(X) = 5$.\\
What is the conditional variance $Var(X - 8 | X > 4)$?\\
In the conditional model (i.e., given that $X>4$), the random variables $X-4$ and $X-8$ differ by a constant. Hence they have the same variance and the answer is again 5.
\newpage
\section{Exercise: Joint PMF calculation}
The random variable $v$ takes values in the set $\{0,1\}$ and the random variable $w$ takes values in the set $\{0,1,2\}$. Their joint PMF is of the form\\
$p_{V, W}(v,w)=c * (v+w)$,\\
where $c$ is some constant, for $v$ and $w$ in their respective ranges, and is zero everywhere else.\\
Find the values of $c$\\
$c = \frac{1}{9}$\\
The sum of the entries of the PMF is $c*(0+0)+c*(0+1)+c*(0+2)+c*(1+0)+...=9c$. Since this sum must be equal to 1, we have $c=1/9$.\\
\\ 
Find $p_v(1)$\\
$p_v(1) = \frac{1}{9} * (1 + 2 + 3) = \frac{6}{9}$

\end{document}