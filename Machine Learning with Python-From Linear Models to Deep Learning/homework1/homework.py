#python package
import numpy as np
from itertools import chain
import matplotlib.pyplot as plt

# Part 1

def perceptron(x, y, w0=0, start=0):
    """
    Decision boundary: wx(i) + w0 = 0
    """

    n = x.shape[0]
    w = np.zeros(2)
    weights = []  # updating progress
    mistake = 0 # number of mistake
    while True:
        converged = True
        for i in chain(range(start, n), range(start)):
            if y[i] * (w.dot(x[i]) + w0) <= 0:  # misclassified
                mistake += 1
                w += y[i] * x[i]  # update weight
                weights.append(w.tolist())  # capture intermediate weight
                converged = False

        if converged:
            break

    return weights, mistake


x = np.array([[-1, -1], [1, 0], [-1, 1.5]])
y = np.array([1, -1, 1])
print(perceptron(x, y))
print(perceptron(x, y, start=1))


x2 = np.array([[-1,-1], [1,0], [-1, 10]])
y2 = np.array([1, -1, 1])
print(perceptron(x2, y2))
print(perceptron(x2, y2, start=1))


plt.scatter(x[:, 0], x[:, 1], color="r", marker='^')
plt.scatter(x2[:, 0], x2[:, 1], color="b", alpha=.4)
plt.show()

# Part 2

def misclassify(x, y, w=None, iters=1000):
    """
    Decision boundary: wx(i) = 0
    w: if None, initialize to 0
    """

    n, d = x.shape
    x = np.column_stack((np.ones(n), x))  # add preceding 1's
    if w is None:
        w = np.zeros(d + 1)  # add preceding offset
    else:
        w = np.array(w, dtype=float)

    counts = np.zeros(n, dtype=int)  # count of mistakes on each point
    mistakes = []  # intermediate mistakes
    weights = []  # intermediate weights
    for k in range(iters):
        converged = True
        for i in range(n):
            if y[i] * w.dot(x[i]) <= 0:  # misclassified
                w += y[i] * x[i]  # update weight
                counts[i] += 1  # update mistake count
                weights.append(w.tolist())  # capture intermediate weight
                mistakes.append(counts.tolist())  # capture intermediate mistakes
                converged = False

        if converged:
            break

    return mistakes, weights


x3 = np.array([[-4, 2], [-2, 1], [-1, -1], [2, 2], [1, -2]])
y3 = np.array([1, 1, -1, -1, -1])
mistakes, weights = misclassify(x3, y3)
print("mistakes : ", mistakes)
print("weights : ", weights)



mistakes, weights = misclassify(x3, y3, w=(-10, -10, 10))
print("mistakes2 : ", mistakes)
print("weights2 : ", weights)